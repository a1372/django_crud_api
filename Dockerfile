# syntax=docker/dockerfile:1
FROM ubuntu:18.04

RUN mkdir /crud_api
COPY ./ /crud_api
RUN ls /crud_api

RUN apt-get update && \
    apt-get install -y libpq-dev gcc curl nano bash ssh python3-pip && \
    apt-get install -y python3.6 && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /crud_api

RUN pip3 install -r requirements.txt
RUN chmod +x run.sh

ENV DISPLAY=:0

CMD ./run.sh