from django.urls import path
from .views import UsersView, TasksView


urlpatterns = [
    path('users/', UsersView.as_view()),
    path('tasks/', TasksView.as_view())
]