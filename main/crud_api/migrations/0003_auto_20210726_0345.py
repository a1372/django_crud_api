# Generated by Django 3.2.5 on 2021-07-26 03:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crud_api', '0002_alter_usertable_first_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='taskstable',
            name='todo',
        ),
        migrations.AddField(
            model_name='taskstable',
            name='description',
            field=models.TextField(default=None, null=True),
        ),
    ]
