from django.db import models
import datetime


class UserTable(models.Model):

    first_name = models.CharField(max_length=255, null=True, default=None)
    last_name = models.CharField(max_length=255)
    department = models.CharField(max_length=255)
    created_at = models.DateTimeField(default=datetime.datetime.now)

    class Meta:
        db_table = 'users'


class TasksTable(models.Model):

    name = models.CharField(max_length=255)
    description = models.TextField(default=None, null=True)
    status = models.IntegerField(default=0)
    assignee = models.ForeignKey(UserTable, on_delete=models.CASCADE, null=True, default=None)
    created_at = models.DateTimeField(default=datetime.datetime.now)
    dead_line = models.DateTimeField()

    class Meta:
        db_table = 'tasks'
