from rest_framework import serializers
from .models import *


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserTable
        fields = '__all__'
        

class TasksSerializer(serializers.ModelSerializer):
    class Meta:
        model = TasksTable
        fields = '__all__'
